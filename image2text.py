#!/usr/bin/python3
import math
import sys
from pathlib import Path
from PIL import ImageFont
from PIL import Image
from PIL import ImageDraw

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

HSIZE = 15
WSIZE = 8
FONT = "FreeMono.ttf"


# With a font type and a char we get the image of the char being printed as an image.
def raster_char(font_type, char):
    font = ImageFont.truetype(font_type, 14)
    ascent, descent = font.getmetrics()
    (width, baseline), (offset_x, offset_y) = font.font.getsize(char)
    image = Image.new("RGB", (width, ascent + descent), WHITE)
    draw = ImageDraw.Draw(image)
    draw = draw.text((0, 0), char, BLACK, font=font)
    del draw
    image = image.convert('L')
    return image


def resize_dimensions(dimensions, scalefactor):
    return (int(dimensions[0] / scalefactor), int(dimensions[1] / scalefactor * 8 / 17))


# Return the size of a set
def get_set_size(s):
    return sum(1 for _ in s)


# Returns the distance between two pixels
def pixel_distance(pixel1, pixel2):
    dist = 0
    if isinstance(pixel1, int) and isinstance(pixel2, int):
        dist = abs(pixel1 - pixel2)
    else:
        dist = math.sqrt(sum([(x - y) ** 2 for x, y in zip(pixel1, pixel2)]))
    return dist


# Equalize the histogram of an image.
def histogram_equalization(image):
    colors = get_image_colors(image)
    # print(f'{colors=}')
    colors_count = get_set_size(colors)
    colors_sorted = sorted(list(colors))
    delta_color = 255 / max(colors_count - 1, 1)
    color_mapping = {}
    for idx, color in enumerate(colors_sorted):
        color_mapping[color] = int(idx * delta_color)
    image.putdata([color_mapping[pixel] for pixel in list(image.getdata())])
    return image


# creates a dictionary with the character and the average value of the character.
def init_letters() -> list:
    # The characters between 10241 and 10496 are the braille characters
    return [(chr(x), list(raster_char(FONT, chr(x)).getdata())) for x in list(range(10240, 10496, 1))]
    # return [(chr(x),list(raster_char(FONT, chr(x)).getdata())) for x in list(range(32, 127, 1))]


# Given an image return all the unique pixels.
def get_image_colors(image):
    colors = set()
    colors.update(list(image.getdata()))
    return colors


# What rasterized image of a char aproximates better an image(both have the same size).
def get_closest_letter(image, letters):
    distance = 35000.0
    bestchar = None
    img_data = list(image.getdata())
    for letter, letter_data in letters:
        newdist = sum([pixel_distance([x], [y]) for x, y in zip(img_data, letter_data)])
        if newdist < distance:
            distance = newdist
            bestchar = letter
    return bestchar


# Convert an image to text but using the similarity of a rasterized character.
def img2ascii(image, scalefactor=None):
    img = Image.open(image)
    (width, height) = img.size
    if scalefactor:
        img = img.resize((int(scalefactor * width), int(scalefactor * height)))
    img = img.convert('L')
    (width, height) = img.size

    letters = init_letters()
    img = histogram_equalization(img)
    for h in range(int(height / HSIZE)):
        for w in range(int(width / WSIZE)):
            subimage = img.crop((w * WSIZE, h * HSIZE, (w + 1) * WSIZE, (h + 1) * HSIZE))
            print(get_closest_letter(subimage, letters), end="")
            # exit()
            # print(get_closest_img(subimage,letters), end="")
        print()



if len(sys.argv) < 2:
    print('This program needs 2 parameters.')
else:
    if Path(FONT).is_file():
        if len(sys.argv) == 2:
            img2ascii(sys.argv[1])
        else:
            img2ascii(sys.argv[1], float(sys.argv[2]))
    else:
        print('Font file ' + FONT + ' is missing.')
